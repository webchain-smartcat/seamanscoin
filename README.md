# SeamansCoin

## SeamansClub

Seamans Clubs - different names, in different cultures and era, but same mission, service and function- have been around from ancient times to cater Sailors, Seamen, Pirates, Navy, passengers....everybody who travels by or operates a ship. There are over 5000 ports in the world. Seamans Clubs exist in the major global ports, with a few of them having web presence!

## SeamansCoin

SeamansClub in collaboration with SmartCat, Inc is creating an innovative coin, SeamansCoin based on the digital technology called Webchain. Purpose of the this coin to provide integrated global marine economy platform for industries like

-   Logitstics and Distribution businesses
-   Used ship and yacht trading businesses
-   Culture and tourisms
-   Promotion and education
-   Marine biology conservation

## Whitepaper

Please download whitepaper from here or visit us at https://seamansclub-cat.com/

## Contact

Website: https://seamansclub-cat.com/
Email: info@seamansclub-cat.com
